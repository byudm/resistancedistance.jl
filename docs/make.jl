using Documenter
using LightGraphs, ResistanceDistance

push!(LOAD_PATH, "../src")

makedocs(
    modules  = [ResistanceDistance],
    sitename ="ResistanceDistance",
    pages    = Any[
        "Home"           => "index.md",
        "Core Functions" => "corefunctions.md"
    ]
)

deploydocs(
    repo   = "gitlab.com/spectral-graph-theory/resistancedistance.jl",
    branch = "gl-pages",
    target = "public"
)

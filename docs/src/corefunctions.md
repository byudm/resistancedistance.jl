# Core Functions
*ResistanceDistance.jl* currently includes the following set of functions designed
to calculate resistance distance and spanning trees/forests with naive techniques.

## Resistance Distance

```@meta
CurrentModule = ResistanceDistance
```

```@autodocs
Modules = [ResistanceDistance]
```

# Floating-point Error
Due to using naive techniques to compute most quantities, several functions
throw errors if they are used on a class of graphs with several vertices (the
critical order of a graph seems to be around 76). However, the naive methods
work well for simple computations that are allowed to be within a margin of error
of 0.001.

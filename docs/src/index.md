# ResistanceDistance

*ResistanceDistance.jl* offers simple tools for computing resistance distance and
related data on graphs. This package uses graphs from [LightGraphs.jl](https://github.com/JuliaGraphs/LightGraphs.jl). Goals for
the project include adding more functions related to computing resistance distance
and improving the accuracy and performance of the basic set of functions that
are currently provided.

Exact precision is offered for most functions, but is only guaranteed for smaller
graphs due to floating-point errors. A major goal of this project is to minimalize
or completely eliminate these errors.

##  Future of the Project
Research on resistance distance is evolving rapidly, it is possible that
we begin to implement methods for calculating resistance distance using two-separations
and even providing methods that offer fast and high-precision results by using
explicit formulas that many authors have been deriving for large classes of graphs.

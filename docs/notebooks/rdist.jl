### A Pluto.jl notebook ###
# v0.12.17

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ 1fadbece-1e2c-11eb-2be0-171cf84fd9b2
begin
	import Pkg
	
	Pkg.activate(mktempdir())
	Pkg.add(["LightGraphs", "GraphPlot", "Colors", "LinearAlgebra"])
	Pkg.add(Pkg.PackageSpec(url="https://gitlab.com/byudm/ResistanceDistance.jl"))
	
	using PlutoUI, ResistanceDistance, LinearAlgebra, LightGraphs, GraphPlot, Colors
end

# ╔═╡ f3e23d0e-0ea1-11eb-23ee-7d8b35da2ee4
md"""
# Resistance Distance
_Resistance distance_ or _effective resistance_ is a metric on a graph
where the distance between $i, j \in V(G)$ is measured by _Kirchoff's loop
rules_. Mathematically, if $L$ is the
[_laplacian matrix_](https://mathworld.wolfram.com/LaplacianMatrix.html) of $G$
and $L^\dagger$ is the 
[_Moore-Penrose inverse_](https://mathworld.wolfram.com/Moore-PenroseMatrixInverse.html) of $L$, we may measure resistance from $i$ to $j$ as
$\renewcommand{\vec}{\mathbf}$

$$
\begin{equation*}
	r_G(i, j) = (\vec e_i - \vec e_j) L^\dagger (\vec e_i - \vec e_j).
\end{equation*}$$

The metric of effective resistance is a measure of both the connectivity and 
distance between two vertices; a large resistance corresponds to a vertex pair 
that is both distant and not well-connected, while smaller resistances correspond 
to vertex pairs that may be close or well-connected.

ResistanceDistance.jl offers a single function `resistance_distance` or `rdist` 
for computing effective resistance on connected graphs. By setting 
`rationalize=true`, it's even possible to get resistance as a rational value.
"""

# ╔═╡ aa5cb726-1668-11eb-02b0-5fa982043d1b
md"""
#### Example
The parameters below control the type and number of vertices of the graph and the
selected vertices `i` and `j`.\
Graph: $(@bind g Select(["c" => "cycle", "k" => "complete", "s" => "star", "p" => "path", "w" => "wheel"]))
Vertices: $(@bind n Slider(3:20))
i: $(@bind i NumberField(1:20))
j: $(@bind j NumberField(1:20, default=2))
"""

# ╔═╡ 1ffd11fe-0eff-11eb-04d8-251ba46f5511
begin
	G, layout = if g == "c"
		cycle_graph(n), circular_layout
	elseif g == "k"
		complete_graph(n), spring_layout
	elseif g == "s"
		star_graph(n), spring_layout
	elseif g == "w"
		wheel_graph(n), spectral_layout
	else
		path_graph(n), circular_layout
	end
	
	error(msg) = Markdown.MD(Markdown.Admonition("danger", "Error!", [msg]))
		
	try	
		local nodefillc = fill(RGB(0., 0.5, 0.), n)
		nodefillc[i] = nodefillc[j] = RGB(1., 0.65, 0.)
		local nodelabel = ["$i" for i=1:nv(G)]
		nodelabel[i] = "i = $(nodelabel[i])"
		nodelabel[j] = "j = $(nodelabel[j])"
		plt = gplot(G, layout=layout, nodefillc=nodefillc, nodelabel=nodelabel)
	catch e
		tmp = if i > n "i" else "j" end
		error(md"The selected vertex $tmp is out of bounds.")
	end
end

# ╔═╡ 23710738-1e30-11eb-2ff8-9d15d6bc9bf9
md"
To find the effective resistance between `i` and `j`, we use `rdist`:
"

# ╔═╡ 345dd1c4-166b-11eb-20cf-2d13c04d396d
rdist(G, i, j, rationalize=false)

# ╔═╡ c0963bd2-36b8-11eb-2ada-4b2c4dc6337b
md"
## Generalization to all Graphs
Leonhard et al were able to provide a more general definition of effective resistance
in their work [A New Notion of Effective Resistance for Directed Graphs]
(https://arxiv.org/pdf/1310.5168.pdf). We provide a brief summary of their results 
below.

For the remainder of the section, we let ``G`` denote a graph on ``n`` vertices 
and labelled ``\{1, 2, \ldots, n\}`` and when we refer to the laplacian matrix of the 
graph, the matrix ``D`` in the equation ``L = D - A`` is the diagonal matrix whose 
``(i, i)``-th entry is the outdegree of vertex ``i``.

To compute resistance between ``i, j \in V(G)``, we must find a matrix ``X`` 
satisfying
```math
	X L = L X = \Pi \quad\text{and}\quad X \Pi = \Pi X = X
```
where
```math
	\Pi_n = I_n - \frac 1n \mathbf 1 \mathbf 1'
```
is the orthogonal projection matrix onto ``\DeclareMathOperator{\span}{span}
\span\{\mathbf 1\}^{\perp}``. and ``Q`` be an ``n-1 \times n`` matrix
such that ``QQ' = I_{n-1}`` and ``Q'Q = \Pi_n``. The matrix ``\overline{L} = Q L Q``
"

# ╔═╡ 1a324db8-36ba-11eb-0a70-210512e6a184
Π(n::T) where T <: Integer=I(n) - fill(1/n, n, n)

# ╔═╡ 104c533e-36bc-11eb-089e-f5a77f7a08e7
"""
	projection(n) → Q

Returns the orthogonal projection matrix onto the subspace of ``M_{(n-1) \\times n}
(\\Bbb R)`` orthogonal to ``\\mathbf 1``.
"""
function projection(n::T) where T <: Integer
	res = zeros(Float64, n-1, n)

	for i=1:(n-1)
		c = 1/(sqrt(n-i + (n-i)^2))
		
		for j=1:(n-i)
			@inbounds res[i,j] = -c
		end
		
		@inbounds res[i, n-i+1] = (n-i)*c
	end
	
	res
end

# ╔═╡ a83a6954-0ea5-11eb-2eb2-2102633aa8e7
md"
## Kirchhoff's Loop Laws
Consider $i, j \in V(G)$. If there exists no path from $i$ to $j$, we say that the 
resistance from $i$ to $j$ is infinite, and if $i=j$, the effective resistance is 
$0$. Otherwise, resistance must be computed with the following graph reductions:

1. If $p$ is a path from $i$ to $j$, replace $p$ with a single weighted edge $(i, j, w(p))$, where $w(p)$ is the sum of the weights across $p$.
2. If $e_1, e_2$ are two nonequal weighted edges from $i$ to $j$, replace them with a single edge with weight $1/(1/w(e_1) + 1/(w(e_2))$

Performing the reductions above until a single weighted edge of the form
$e = (i, j, w)$ remains, we have $r_G(i, j) = w$."

# ╔═╡ f4518d22-0ef5-11eb-0301-8d9a1f445493
md"""
## Spanning Trees and Forests
A _spanning tree_ is a subgraph ``T`` of ``G`` that is a tree containing every vertex
of ``G`` and a _spanning ``n``-forest_ is a subgraph ``\mathcal F`` of ``G`` 
containing ``n`` graph components, each one a tree, such that ``\mathcal F`` contains 
every vertex of ``G.`` Often, we wish to find a spanning forest _separating_ a set of
``n`` vertices, which has the additional condition that given some set of vertices
``\mathcal V,`` each component of the spanning forest contains a distinct vertex
``v \in \mathcal V.`` Given a collection of vertices, ``\{v_i\}_{i=1}^n,`` we
typically denote the spanning forest separating the vertices as 
``\mathcal F_G(v_1, v_2, \ldots, v_n).``
"""

# ╔═╡ 02de82cc-1672-11eb-101c-49705b64fc78
md"""
### The Matrix-Tree Theorem
We may compute the number of spanning ``n``-forests separating a set of ``n`` distinct
vertices ``\mathcal V`` in polynomial time by taking the determinant of the ``i``-th minor of the laplacian, ``i \in \mathcal V.``
"""

# ╔═╡ e20f4564-19a9-11eb-20b7-cd4409f1fd29
Markdown.MD(Markdown.Admonition("warning", "Warning!", [md"
Although the Matrix-Tree theorem makes counting spanning trees/forests
computationally tractable, even the best matrix algorithms require ``\mathcal O(n^2)`` 
operations to compute determinants. The most performant algorithm that Julia offers 
for computing determinants of sparse matrices uses floating-point arithmetic, and the 
number of spanning trees/forests given by the methods below are often innacurate."]))

# ╔═╡ 46fbd73a-166c-11eb-3fd2-797426bfc679
md"""
#### Example
We wish to find the number of spanning trees of the graph ``G`` defined in the
previous section, shown below for the reader's convenience:

$(gplot(G, nodefillc=RGB(0., 0.5, 0.), layout=layout))

`ResistanceDistance` offers the function `trees` for computing the number of
spanning trees of ``G`` using the above method
"""

# ╔═╡ 8bf21ba8-166d-11eb-072c-1b0cac73e985
trees(G)

# ╔═╡ 80f1054a-18a7-11eb-1034-e1529e1ba666
md"""and the function `forests` for computing the number of spanning ``n``-forests
separating ``i`` and ``j.``"""

# ╔═╡ 8fa852c0-166d-11eb-0c7b-4b784ec8af8e
forests(G, i, j)

# ╔═╡ e839b2e4-1670-11eb-1a9b-1965271dcfd4
md"""
### Applications to Effective Resistance
Renowned graph theorist Ravindra B. Bapat proved that effective resistance satisfies
```math
	r_G(i, j) = \frac{\mathscr F_G(i,j)}{T(G)}.
```
where ``T(G)`` is the number of spanning trees in ``G`` and ``\mathscr F_G(i, j)`` is the number of spanning two forests separating ``i`` and ``j`` in ``G.``
"""

# ╔═╡ ebc42e6e-1a1a-11eb-1d47-150c4a0ddfa5
md"""
### Two-forest and Resistance Matrices
The matrix ``S`` whose ``(i, j)``-th entry is ``\mathscr F_G(i, j)`` is know as the
two-forest matrix of ``G.`` If we divide ``S`` by ``T(G),`` we get a matrix ``R``
whose ``(i, j)``-th entry is ``r_G(i, j),`` often referred to as the resistance matrix
of ``G.``
"""

# ╔═╡ 827b34a2-1a1a-11eb-26be-4f38b3237c6c
S = [forests(G, i, j) for i=1:nv(G), j=i:nv(G)]

# ╔═╡ d1964194-1a1a-11eb-1cac-83874a912d88
R = S//trees(G)

# ╔═╡ 4d4b77a0-1a1b-11eb-2c87-2799ef2107e4
md"""
`ResistanceDistance` contains the optimized function `resistance_matrix` to simplify
the previous computations.
"""

# ╔═╡ 63e9969a-1a1b-11eb-321d-edce3cce4788
resistance_matrix(G; rationalize=false)

# ╔═╡ b0119eae-1675-11eb-1d83-590de8e679a2
md"""
## Graph Separations
An _``n``-separation_ on a graph ``G`` is a pair of subgraphs ``G_1, G_1`` where
- ``G_1`` and ``G_2`` together contain all of the vertices and edges in ``G``
- ``G_1`` and ``G_2`` share exactly ``n`` vertices, called the _``n``-separator_ of ``G``
- There are no common edges between ``G_1`` and ``G_2``
"""

# ╔═╡ 30ef0262-2253-11eb-2035-0929ba4c0fd6
md"
## Connection to Other Fields
Resistance distance is not only an important measure of connectivity on a graph, it
also has connections to other graph invariants associated to graph connectivity.
"


# ╔═╡ 9fde94d2-2256-11eb-064a-d3d9c9312e91
md"
### Kirchhoff Index
In the study of electric networks, we often wish to know the total resistance of a
graph, an invariant known as the _Kirchhoff index._ Formally, we compute the
Kirchhoff index via the combinatorial counting identity
```math
	Kf(G) = \frac 12 \sum_{i, j \in G} r_G(i, j).
```
The fractional term serves to prevent double-counting resistance across the same
vertex pair. This sum is computationally expensive, so the expression
```math
	\DeclareMathOperator{\trace}{tr}
	Kf(G) = n \trace(L^\dagger)
```
serves as a much faster alternative. The above was originally stated in [Some results on resistance distances and resistance matrices](https://www.tandfonline.com/doi/abs/10.1080/03081087.2013.877011).
Results from the field of combinatorics allow us to find a closed-form expression for 
resistance of any graph for which we may describe resistance between any vertex pair
with a closed-form expression.
"

# ╔═╡ bdda2f6e-2256-11eb-2aed-4f43234d0327
md"
### Kemeny's Constant
_Kemeny's constant_ is an invariant in the study of _finite ergodic Markov chains_.
We do not provide a complete explanation of Markov chains here, but will instead
refer the curious reader to the comprehensive article [here](https://brilliant.org/wiki/markov-chains/).
It is sufficient to know that Kemeny's constant is defined and bounded for random 
walks on connected nonbipartite graphs. If ``G`` is a simple connected graph, the
Kemeny's constant of a random walk on the vertices satisfies
```math
	\DeclareMathOperator{\kemeny}{\mathcal K}	
	\kemeny(G) = \frac{d' R d}{4m} = \frac{1}{4m} \sum_{i, j \in G} d_i d_j r_G(i, j),
```
where ``d`` is the degree vector of ``G,`` ``R`` is the resistance matrix, and ``m``
is the number of edges in the graph.

Due to the wide range of applications of Markov chains and random walks, Kemeny's
constant is seeing an increasing relevance in the study of graph theory, and similar
to the Kirchoff index, if we are able to describe graph resistance with closed form
expressions, we are also able to find closed form expressions for Kemeny's constant.
"

# ╔═╡ Cell order:
# ╟─1fadbece-1e2c-11eb-2be0-171cf84fd9b2
# ╟─f3e23d0e-0ea1-11eb-23ee-7d8b35da2ee4
# ╟─aa5cb726-1668-11eb-02b0-5fa982043d1b
# ╟─1ffd11fe-0eff-11eb-04d8-251ba46f5511
# ╟─23710738-1e30-11eb-2ff8-9d15d6bc9bf9
# ╠═345dd1c4-166b-11eb-20cf-2d13c04d396d
# ╟─c0963bd2-36b8-11eb-2ada-4b2c4dc6337b
# ╠═1a324db8-36ba-11eb-0a70-210512e6a184
# ╠═104c533e-36bc-11eb-089e-f5a77f7a08e7
# ╟─a83a6954-0ea5-11eb-2eb2-2102633aa8e7
# ╟─f4518d22-0ef5-11eb-0301-8d9a1f445493
# ╟─02de82cc-1672-11eb-101c-49705b64fc78
# ╟─e20f4564-19a9-11eb-20b7-cd4409f1fd29
# ╟─46fbd73a-166c-11eb-3fd2-797426bfc679
# ╠═8bf21ba8-166d-11eb-072c-1b0cac73e985
# ╟─80f1054a-18a7-11eb-1034-e1529e1ba666
# ╠═8fa852c0-166d-11eb-0c7b-4b784ec8af8e
# ╟─e839b2e4-1670-11eb-1a9b-1965271dcfd4
# ╟─ebc42e6e-1a1a-11eb-1d47-150c4a0ddfa5
# ╠═827b34a2-1a1a-11eb-26be-4f38b3237c6c
# ╠═d1964194-1a1a-11eb-1cac-83874a912d88
# ╟─4d4b77a0-1a1b-11eb-2c87-2799ef2107e4
# ╠═63e9969a-1a1b-11eb-321d-edce3cce4788
# ╟─b0119eae-1675-11eb-1d83-590de8e679a2
# ╟─30ef0262-2253-11eb-2035-0929ba4c0fd6
# ╟─9fde94d2-2256-11eb-064a-d3d9c9312e91
# ╟─bdda2f6e-2256-11eb-2aed-4f43234d0327

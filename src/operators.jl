export separate

"""
    separate(G, v...)

Separates `G` into two components using `v` as a list of separators.
If no separation is possible, returns two empty graphs.

!!! note
    Partitioning and separating a graph are two distinct operations; graph
    partitioning allows for the removal of edges, while a graph separation
    must include each edge in the graph exactly once.

# Examples
```jldoctest
julia> using LightGraphs, ResistanceDistance;

julia> G = wheel_graph(7);

julia> separate(G, 2, 7)
({7, 11} undirected simple Int64 graph, {2, 1} undirected simple Int64 graph)
```
"""
function separate(G::SimpleGraph{T}, v::T...) where T <: Integer
    !allunique(v) && throw(ArgumentError("vertex separators must be unique"))
    len = size(v, 1)
    G₁  = SimpleGraph{T}(len)
    G₂  = SimpleGraph{T}(len)
    V₁  = zeros(T, nv(G))
    V₂  = zeros(T, nv(G))

    for (i, u) in enumerate(v)
        @inbounds V₁[u] = i
        @inbounds V₂[u] = i
    end

    blacklist = [i for i in v]

    for u in v
        for n in neighbors(G, u)
            H, V = if nv(G₂) < nv(G₁) G₂, V₂ else G₁, V₁ end

            if n ∉ blacklist
                push!(blacklist, n)

                _add_edge!(H, V, n, u)
                _add_cluster!(H, V, G, n, blacklist)
            elseif n ∈ v
                _add_edge!(H, V, n, u)
            end
        end
    end

    G₁, G₂
end

function separate(G::SimpleGraph{T}, v::T) where T <: Integer
    G₁  = SimpleGraph{T}(1)
    G₂  = SimpleGraph{T}(1)
    V₁  = zeros(T, nv(G))
    @inbounds V₁[v] = 1
    V₂ = copy(V₁)
    blacklist = [v]

    for n in neighbors(G, v)
        H, V = if nv(G₂) < nv(G₁) G₂, V₂ else G₁, V₁ end

        if n ∉ blacklist
            push!(blacklist, n)

            _add_edge!(H, V, n, v)
            _add_cluster!(H, V, G, n, blacklist)
        end
    end

    G₁, G₂
end

# Helper functions for performing partitions and separations
function _add_cluster!(H::SimpleGraph{T}, vmapping::Array{T, 1}, G::SimpleGraph{T},
                      u::T, blacklist::Array{T, 1}) where T <: Integer
    if degree(G, u) == 1
        return
    end

    for n in neighbors(G, u)
        if n == u
            continue
        end

        _add_edge!(H, vmapping, u, n)

        if n ∉ blacklist
            push!(blacklist, n)
            _add_cluster!(H, vmapping, G, n, blacklist)
        end
    end
end

function _add_edge!(G::SimpleGraph{T}, vmapping::Array{T, 1}, u::T, v::T) where T <: Integer
    if vmapping[u] == 0
        add_vertex!(G)
        vmapping[u] = nv(G)
    end

    if vmapping[v] == 0
        add_vertex!(G)
        vmapping[v] = nv(G)
    end

    add_edge!(G, vmapping[u], vmapping[v])
end

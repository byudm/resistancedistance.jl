export spanning_trees, spanning_forests

"""
	spanning_trees(G)

Returns the number of spanning trees in `G`. Throws an `ArgumentError` if `G`
is not connected. This function is only guaranteed to be accurate for |G| ≤ 70.

!!! warning
    Throws an `InexactError` for most |G| > 70.

# Examples
```julia-repl
julia> G = cycle_graph(3);

julia> spanning_trees(G)
5
```
"""
function spanning_trees(G::AbstractGraph)
    !is_connected(G) && return 0

    n = nv(G)
    L = laplacian_matrix(G)
    L = @inbounds L[1:(n-1), 1:(n-1)]

    trunc(Int128, det(L))
end

"""
    spanning_forests(G, v₁...)

Finds the number of spanning n-forests separating the vertices of `v...` in `G`.
Throws an `ArgumentError` if `G` is not connected and a `DomainError` if one or
more of the vertices are not in the vertex set of `G`.

!!! warning
    This function is inaccurate for most graphs with |G| > 70 and often throws
    an `InexactError`

# Examples
```julia-repl
julia> G = cycle_graph(G);

julia> spanning_forests(G, 1, 3)
2
```
"""
function spanning_forests(G::AbstractGraph{T}, v::T...) where T <: Integer
    len   = size(v, 1)
    order = nv(G)
    len < 2     && throw(ArgumentError("There must be at least two vertices to create a spanning forest"))
    len > order && throw(ArgumentError("G has order $order but $len vertices were passed in"))

    @boundscheck (for i in v
        !has_vertex(G, i) && throw(DomainError("The graph does not contain vertex $i"))
    end)

    if !allunique(i for i in v)
        return 0
    end

    L = laplacian_matrix(G)
    slice = fill(true, order)
    @inbounds for i in v
        slice[i] = false
    end

    trunc(Int128, det(@inbounds L[slice, slice]))
end

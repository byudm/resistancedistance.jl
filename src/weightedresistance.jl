export kemenys_constant

import SimpleWeightedGraphs: SimpleWeightedGraph, degree_matrix, adjacency_matrix

function kemenys_constant(G::SimpleWeightedGraph{T, U}) where T <: Integer where U <: Real
    !is_connected(G) && return Inf

    M = inv(convert(Matrix{U}, degree_matrix(G))) * adjacency_matrix(G)
    λ = real(eigvals(M)[1:nv(G)-1])

    sum(x -> 1/(1-x), λ)
end

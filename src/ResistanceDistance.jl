"""
    ResistanceDistance

Methods for working with effective resistance and various graph metrics and
invariants of electric network theory.
"""
module ResistanceDistance

import LightGraphs: # Types
                    AbstractGraph, AbstractEdge, DiGraph, SimpleGraph,
                    # Matrices associated with graphs
                    laplacian_matrix, adjacency_matrix, non_backtracking_matrix,
                    # Boolean operators
                    has_vertex, has_edge, has_path, is_connected, is_directed,
                    # Vertex/edge accessors
                    degree, outdegree, outneighbors, inneighbors, neighbors,
                    vertices, edges, nv,
                    # Graph mutators
                    add_vertex!, add_vertices!, add_edge!, rem_vertex!

import LinearAlgebra: # Types
                      Diagonal, Symmetric,
                      # Eigen operators
                      eigvals, eigvals!, eigvecs,
                      # Matrix operations
                      inv, pinv, det, tr

export resistance_distance, rdist, max_resistance, maxres, kirchhoff_index,
       kind, kemenys_constant, kconst, resistance_matrix, spanning_trees,
       trees, spanning_forests, forests, separate

# On-demand loading of functions for weighted graphs
function __init__()
    if @isdefined SimpleWeightedGraphs
        include("weightedresistance.jl")
    end
end

include("operators.jl")
include("forests.jl")

# Function aliases for included files
trees=spanning_trees
forests=spanning_forests

"""
	resistance_distance(G, i, j; rationalize) → ``r_G(i, j)``

Returns the resistance distance between vertex `i` and `j` in `G`.
Throws an error if `G` is not connected or one of the vertices is not in `G`.

# Optional keyword arguments
- `rationalize` - if `true`, returns the effective resistance as a rational
  number.

!!! warning
    If called with `rationalize=true`, throws an `InexactError` for most graphs |G| > 70.

# Examples
```jldoctest
julia> using LightGraphs, ResistanceDistance;

julia> g = cycle_graph(3);

julia> resistance_distance(g, 1, 3)
0.6666666666666667
```
"""
function resistance_distance(G::AbstractGraph{T}, i::T, j::T) where T <: Integer
    !has_vertex(G, i) && throw(DomainError("G does not contain the vertex $i"))
    !has_vertex(G, j) && throw(DomainError("G does not contain the vertex $j"))
    is_directed(G) && throw(ArgumentError("G must be undirected"))

    if i == j
        return 0
    elseif !has_path(G, i, j)
        return Inf
    end

    L = convert(Array{T, 2}, laplacian_matrix(G))
    M = pinv(L)

    @inbounds(M[i, i] + M[j, j] - 2*M[j, i])
end

resistance_distance(G::AbstractGraph, i, j; rationalize=false)=
    if rationalize
        forests(G, promote(i, j)...) // trees(G)
    else
        resistance_distance(G, promote(i, j)...)
    end

rdist=resistance_distance

"""
	max_resistance(G) → max

Returns the maximum resistance of `G`. If `G` is not connected, returns `Inf`.

# Optional keyword arguments
- `rationalize` - if set to `true`, computes maximum resistance as a rational number

!!! warning
    Calling the function with `rationalize=true` has serious repercussions.
    For most |G| > 70, `max_resistance` throws an `InexactError` and for
    |G| > 50 there is a serious performance hit resulting in computation
    time exceeding several seconds and memory allocations achieving 2 GiB or more.

# Examples
```jldoctest
julia> using LightGraphs, ResistanceDistance;

julia> G = path_graph(10);

julia> max_resistance(G)
9.00000000000002
```
"""
function max_resistance(G::AbstractGraph; rationalize=false)
    !is_connected(G) && return Inf

    mat = resistance_matrix(G; rationalize=rationalize)

    findmax(mat)[1]
end

maxres=max_resistance

"""
	kirchhoff_index(G)

Compute the Kirchhoff index of `G`.

!!! warning
    Suffers from a serious performance hit for |G| > 1700

# Examples
```jldoctest
julia> using LightGraphs, ResistanceDistance;

julia> G = star_graph(10);

julia> kirchhoff_index(G)
81.0
```
"""
function kirchhoff_index(G::AbstractGraph{T}) where T <: Integer
    !is_connected(G) && return Inf

    n = nv(G)
    L = convert(Matrix{T}, laplacian_matrix(G))
    λ = eigvals(L)[2:n]

    n * sum(x -> 1/x, λ)
end

kind=kirchhoff_index

"""
    kemenys_constant(G)

Computes Kemeny's constant of `G`.

# Examples
```jldoctest
julia> using LightGraphs, ResistanceDistance;

julia> G = cycle_graph(38);

julia> kemenys_constant(G)
240.49999999999872
```
"""
function kemenys_constant(G::AbstractGraph)
    !is_connected(G) && return Inf

    M = convert(Matrix{Float64}, adjacency_matrix(G)) ./ outdegree(G)
    λ = real(eigvals!(M)[1:(nv(G)-1)])

    sum(x -> 1/(1-x), λ)
end

kconst=kemenys_constant

"""
    twoforest_matrix(G)

Returns the matrix whose ``(i, j)``-th entry is the number of spanning
twoforests separating ``i`` and ``j.``
"""
function twoforest_matrix(G::AbstractGraph)
    res = Array{Int128}(undef, nv(G), nv(G))
    lapl = laplacian_matrix(G)
    slice = fill(true, nv(G))

    @simd for i = 1:nv(G)
        @simd for j = 1:i
            if j == i
                @inbounds res[i, j] = 0
            else
                @inbounds slice[i] = slice[j] = false
                @inbounds res[j, i] = round(Int128, det(lapl[slice, slice]))
                @inbounds slice[i] = slice[j] = true
            end
        end
    end

    Symmetric(res)
end

"""
	resistance_matrix(G)

Compute the resistance matrix of `G`. Throws an `ArgumentError` if `G` is not
connected. Exact resistances are guaranteed for |G| ≤ 70.

# Optional keyword arguments
- `rationalize` - if `true`, returns a rational matrix

!!! warning
    Calling `resistance_matrix` with `rationalize=true` causes a serious
    performance hit (e.g. several seconds of execution time and multiple
    gigabytes of allocations) for most |G| > 50. This is a __dense__ matrix,
    only call the function if you are confident that you have space for it.

# Examples
```jldoctest
julia> using LightGraphs, ResistanceDistance;

julia> G = cycle_graph(5);

julia> resistance_matrix(G)
5×5 LinearAlgebra.Symmetric{Float64,Array{Float64,2}}:
 0.0  0.8  1.2  1.2  0.8
 0.8  0.0  0.8  1.2  1.2
 1.2  0.8  0.0  0.8  1.2
 1.2  1.2  0.8  0.0  0.8
 0.8  1.2  1.2  0.8  0.0
```
"""
function resistance_matrix(G::AbstractGraph)
    !is_connected(G) && throw(ArgumentError("G must be connected"))
    is_directed(G) && throw(ArgumentError("G must be an undirected graph"))

    n = nv(G)
    M = pinv(Matrix(laplacian_matrix(G)))
    res = zeros(Float64, n, n)

    @simd for i = 1:nv(G)
        @inbounds @simd for j = 1:i
            if i == j
                res[i, j] = 0
            else
                res[j, i] = M[i, i] + M[j, j] - 2M[j, i]
            end
        end
    end

    Symmetric(res)
end

resistance_matrix(G; rationalize=false)=
    if rationalize
        Symmetric(twoforest_matrix(G) // trees(G))
    else
        resistance_matrix(G)
    end

end

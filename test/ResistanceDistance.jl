@testset "ResistanceDistance" begin
	function sunflower_graph(n::T) where T<: Integer
		ne = 3n
		fadjlist = Vector{Vector{T}}(undef, 2n)
		@inbounds fadjlist[1]   = T[2, n, 2n, n+1]
		@inbounds fadjlist[n]   = T[1, n-1, 2n, 2n-1]
		@inbounds fadjlist[n+1] = T[1, 2]
		@inbounds fadjlist[2n]  = T[1, n]

		@inbounds @simd for u = 2:(n-1)
			fadjlist[u]   = T[u-1, u+1, n+u-1, n+u]
			fadjlist[n+u] = T[u, u+1]
		end

		SimpleGraph(ne, fadjlist)
	end

    @testset "resistance_distance" begin
        P_10 = path_graph(10)
        K_7  = complete_graph(7)
        C_30 = cycle_graph(30)
        disconnected = erdos_renyi(71, 65)

        @test resistance_distance(K_7, 3, 3)   == 0
        @test resistance_distance(P_10, 7, 7)  == 0
        @test resistance_distance(P_10, 1, 5)  ≈ 4
        @test resistance_distance(K_7, 1, 4)   ≈ 2/7
        @test resistance_distance(C_30, 2, 14) ≈ 12*18/30
        @test resistance_distance(K_7, 2, 1)   ≈ resistance_distance(K_7, 1, 2)
        for i = 2:71
            if !has_path(disconnected, 1, i)
                @test resistance_distance(disconnected, 1, i) == Inf
                break
            end
        end
    end

    @testset "max_resistance" begin
        K_12    = complete_graph(12)
        P_20    = path_graph(20)
        C_501   = cycle_graph(501)
        SF_1031 = sunflower_graph(1031)
        disconnected = erdos_renyi(18, 13)

        @test max_resistance(K_12)    ≈ 1/6
        @test max_resistance(P_20)    ≈ 19
        @test max_resistance(C_501)   ≈ 250*251/501
        @test max_resistance(SF_1031) ≈ (1031^2 + 4*1031 - 1) / (6*1031)
        @test max_resistance(disconnected) == Inf
    end

    @testset "kirchhoff_index" begin
        K_10   = complete_graph(10)
        P_35   = path_graph(35)
        SF_353 = sunflower_graph(353)
        C_1030 = cycle_graph(1030)
        disconnected = erdos_renyi(71, 65)

        @test kirchhoff_index(P_35)   ≈ 34*35*36/6
        @test kirchhoff_index(SF_353) ≈ (4*(353)^3 + 12*(353)^2 - 7*353) / 18
        @test kirchhoff_index(K_10)   ≈ 9
        @test kirchhoff_index(C_1030) ≈ 1030*1029*1031/12
        @test kirchhoff_index(disconnected) == Inf
    end

    @testset "kemenys_constant" begin
        P_12   = path_graph(12)
        K_57   = complete_graph(57)
        SF_300 = sunflower_graph(300)
        C_5000 = cycle_graph(5000)
        disconnected = erdos_renyi(105, 77)

        if VERSION >= v"1.1"
            @test kemenys_constant(P_12)   ≈ (2*12^2 - 4*12 + 3)/6
            @test kemenys_constant(SF_300) ≈ (300^2 + 2*300 - 1)/3
        end

        @test kemenys_constant(K_57)   ≈ (56)^2/57
        @test kemenys_constant(C_5000) ≈ 5001*4999/6
        @test kemenys_constant(disconnected) == Inf
    end

    @testset "spanning_trees" begin
        disconnected = erdos_renyi(71, 65)
        K_8    = complete_graph(8)
        P_12   = path_graph(12)
        C_77   = cycle_graph(77)

        @test spanning_trees(disconnected) == 0
        @test spanning_trees(K_8)    == 8^6
        @test spanning_trees(P_12)   == 1
        @test spanning_trees(C_77)   == 77
    end

    @testset "spanning_forests" begin
        disconnected = erdos_renyi(71, 65)
    end

    @testset "resistance_matrix" begin
        disconnected = erdos_renyi(71, 65)

        @test_throws ArgumentError resistance_matrix(disconnected)
    end
end

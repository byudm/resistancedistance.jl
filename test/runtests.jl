using ResistanceDistance
using LightGraphs
using LinearAlgebra
using Test

const testdir = dirname(@__FILE__)

tests = [
    "ResistanceDistance"
]

for t in tests
    tp = joinpath(testdir, "$(t).jl")
    include(tp)
end

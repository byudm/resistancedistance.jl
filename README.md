# ResistanceDistance

`ResistanceDistance.jl` is built on top of [LightGraphs](https://github.com/JuliaGraphs/LightGraphs.jl)
with on-demand support for [SimpleWeightedGraphs](https://github.com/JuliaGraphs/SimpleWeightedGraphs.jl)
and offers functions for computing resistance distance and other related metrics, such as the Kirchoff Index
and Kemeny's constant of connected graphs.

## Installation/Usage
`ResistanceDistance` is not a part of the julia package registry, so it must be installed using the
link to this repository:
```julia
julia>] add https://gitlab.com/byudm/ResistanceDistance.jl/
```
## Project Goals
In line with the research that we are doing, we are working on creating functions that compute
resistance distance through 2-separations, which can improve both speed and accuracy of numerical
computations. See
[*Spanning 2-Forests and Resistance Distance in 2-Connected Graphs*](https://arxiv.org/abs/1901.00053) for more
information on computing resistance distance over graphs with 2-separations.

Currently, we wish to implement an algorithm that can identify a 'favorable' 2-separator, compute resistance
distance on the graphs in the 2-separation, then use the 2-separator resistance formulas to compute effective
resistance in the larger graph.

## Progress
We have proven that identifying a favorable 2-separation on a graph is computationally intractable, reducing
to problem that is NP-hard. However, separating a graph using a known 2-separator reduces to a DFS, making
the separation algorithm incredibly efficient.

## Limitations
We have done our best to optimize the code in this module, but most resistance-based computations on graphs
require computing determinants of large matrices or finding Moore-Penrose inverses, so the code is not
very performant on large graphs.

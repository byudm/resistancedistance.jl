# Release Notes

## [ResistanceDistance v0.4.0](https://gitlab.com/byudm/resistancedistance.jl/)
### Added
- Function for finding the twoforest matrix of a graph

### Fixed
- Bug in `kemenys_constant` for `SimpleWeightedGraph` type
- `resistance_distance` no longer works for directed graph types due to the
  psuedoinverse definition of resistance not satisfying necessary properties of
  effective resistance

### Improved
- Refactored `resistance_matrix` for readability

### Docs
- Recent findings on definitions of effective resistance for directed graphs.

## [ResistanceDistance v0.3.1](https://gitlab.com/byudm/resistancedistance.jl/-/tree/v0.3.1)
### Improved
- Slight performance increase for `resistance_distance`, `resistance_matrix`
and `kirchhoff_index`
- Update package dependencies to newest releases

### Docs
- Transition jupyter notebooks to Pluto notebooks
- Docstrings for several functions now have better examples and formatting

## [ResistanceDistance v0.3.0](https://gitlab.com/byudm/resistancedistance.jl/-/tree/v0.3.0)
### Added
- Functions for working with the Kirchhoff index and Kemeny's constant of graphs
- Graph operators for creating 2-separations

### Improved
- Generalized `spanning_forests` to work for any number of vertices
- Several functions now return `Inf` or `0` when called on disconnected graphs instead of throwing an
  `ArgumentError`
- Many functions now work on a larger class of graph types, including weighted and disconnected graphs
- Added `@boundscheck` annotations in appropriate areas

### Fixed
- A bug in `resistance_matrix` causing an error when computing rational terms

## [ResistanceDistance v0.2.0](https://gitlab.com/byudm/resistancedistance.jl/-/tree/v0.2.0)
### Added
- Function for computing the maximum resistance of a graph
- Documentation for all functions

### Improved
- All functions now throw an `ArgumentError` if passed a graph that is not connected

### Changed
- Replaced instances `ArgumentError` with `DomainError` where appropriate

## [ResistanceDistance v0.1.0](https://gitlab.com/byudm/resistancedistance.jl/-/tree/v0.1.0)
### Added
- Functions for working with resistance distance and spanning trees/forests
